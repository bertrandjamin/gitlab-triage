# frozen_string_literal: true

require_relative 'base_policy'
require_relative 'rule_policy'
require_relative '../entity_builders/issue_builder'

module Gitlab
  module Triage
    module Policies
      class SummaryPolicy < BasePolicy
        # Build an issue from several rules policies
        def build_issue
          action = actions[:summarize]

          EntityBuilders::IssueBuilder.new(
            type: type,
            action: action,
            resources: [],
            network: network).tap do |issue|
            issue.items =
              resources.map do |inner_policy_spec, inner_resources|
                Policies::RulePolicy.new(
                  type, inner_policy_spec, inner_resources, network)
                  .build_issue
                  .description
              end.join("\n\n")
          end
        end

        # Due to resources is a different type, this will never work
        # FIXME: We should try to make sure type is consistent for resources
        def comment?
          false
        end

        private

        def attach_resource_type(resources, type)
          resources.each_with_object({}) do |(rule, rule_resources), result|
            result[rule] = super(rule_resources, type)
          end
        end
      end
    end
  end
end
